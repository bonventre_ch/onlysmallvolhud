// ############ [ start ] OnlySmallVolHUD ############

@interface SBElasticVolumeViewController
@end

%hook SBElasticVolumeViewController

-(void)_updateSliderViewMetricsForState:(long long)arg1 bounds:(CGRect)arg2 integralized:(BOOL)arg3 useSizeSpringData:(BOOL)arg4 useCenterSpringData:(BOOL)arg5 {
	%orig(2, arg2, arg3, arg4, arg5);
}

%end

// ############ [ end ] OnlySmallVolHUD ############